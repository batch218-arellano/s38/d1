const mongoose = require("mongoose");

const courseSchema = new mongoose.Schema({
	name:{
		type: String,
		/*
			// Requires the data to be included when creating a record
			// THe "true" value defines if the field is required or not and the second element in the array is the message that will printed out in our terminal when the data is not present
		*/
		required: [true, "Course is required"]
	},
	description:{
		type: String
		required: [true, "Description is required"]
	},
	price:{
		type: Number,
		required: [true, "Price is required"]
	},
	isActive:{
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		// The "new Date()" experession instantiates a new "date" that the current date and time ehenever a course is created in our database
		default: new Date()
	},
	enrollees : [
		{
			userId: {
				type: String,
				required: [true, "UserId is requred"]
			},
			enrolledOn:{
				type: Date,
				default: new Date()
			}
		} 
	]
})

/*
	// Example Database Output
	{
		"name": "BS Information Techonolgy"
		"enrollees": [
			{
				"userId": "user001"	,
				"enrolledOn": "August 1, 2022"
			},
			{
				"userId" : "user002" ,
				"enrolledOn" : "August 2, 2022"
			}

		]
	}
*/
							// Model Name
module.exports = mongoose.model("Course", courseSchema);
